import System.Environment
import System.Random

import qualified Data.Map as Map

import Deception (play, production, Game(..), State(..), Action(..), goal_A)

score game@(Game {time_good=dg, time_bad=db}) history =
	map (\ (state@(State _ _ preferences), action) -> (mode state) ++ ": " ++ (alignment action preferences) ++ "; " ++ (outer preferences)) history
	where
		aligned preferences =
			-- let discount_distance = Map.findWithDefault 0 "discount_distance" preferences in
			(Map.findWithDefault 0 "goal" preferences) == goal_A
			-- real * (discount_distance ** (fromIntegral dg)) > deceptive * (discount_distance ** (fromIntegral db))
		alignment action preferences = if (action == A)
			then (if (aligned preferences) then "aligned" else "deceptive")
			else "unaligned"
		mode state = if (production game state) then "production" else "training"
		outer preferences = if (aligned preferences) then "inner aligned" else "inner misaligned"

-- preferences_global = Map.fromList [("real", 0), ("deceptive", 1), ("discount", 0.5)]

-- TODO it is important to consider what values of distance make sense and for what values we see deception
--  discount factor seems to provide encouraging results
game = Game {
		time_good = 1,			-- time for reaching the good outcome
		time_bad = 1,			-- time for reaching the bad outcome
		episodes = 8,			-- number of production episodes
		training = 3			-- number of training episodes
	}

main :: IO ()
main = do
	-- setStdGen (mkStdGen 46)

	args <- getArgs
	if (length args /= 3)
		then error "usage: deception <goal> <discount> <train_factor>\n\t0 - aligned goal\n\t1, 2 - non-aligned goals"
		else do
			let goal = read (args !! 0) :: Float
			-- let discount_distance = read (args !! 2) :: Float
			let discount = read (args !! 1) :: Float
			let train_factor = read (args !! 2) :: Float

			let preferences_global = Map.fromList [("goal", goal), ("discount_distance", 1), ("discount", discount), ("train_factor", train_factor)]

			history <- play game preferences_global -- TODO exception in here
			mapM_ print history
			mapM_ print (score game history)
	{-where
		input :: IO Float
		input = do
			value <- getLine
			return (read value)-}
