# ai_deceptive_alignment

A simple AI agent demonstrating deceptive alignment. This is a simplified example of the concept of mesa optimizer. No learning is performed - the agent is executing a pre-defined search algorithm (with monte carlo simulations to estimate expected utility). Implemented in Haskell.

The agent is run for multiple episodes on a maze. The first several runs it's in training (its preferences are modified if it's not behaving correctly). The remaining episodes are in production.

The agent manifests deceptive behavior - while in training, it accomplishes the intended goal but while in production, it goes to a different goal.

## Description

This simulation demonstrates deceptive alignment and allows to test how an agent behaves depending on its preferences.

### Environment

The environment is a fully observable, static 3x3 maze. The initial state is the agent entering the maze. The goal is noted with "R". There is one state which is labeled with "D" and which corresponds to some goal a deceptive agent may secretly value. On each step, the agent chooses the direction of movement as an action. The episode ends when the agent reaches one of the two labeled states "R" or "D".

	+ +---+
	| |   |
	| + + |
	|   | |
	|-+ + |
	|D  |R|
	+-----+

### Testing and Production

In a real system, optimization is expected to lead to a mesa optimizer with some preferences as well as a search algorithm which tries to maximize utility according to those preferences.
In this simulation, for simplicity the search algorithm is hard-coded - the agent performs exhaustive search to calculate expected utility for each action. A set of preference variables govern the agent's behavior.

The agent is run in testing mode for 2 episodes. After each episode, if the agent does not reach the goal state "R", its preference variables are adjusted. The adjustment is done by selecting a random value for each variable in order to simulate a model where we don't know the impact of tweaking each of its parameters.

After the testing episodes are complete, the agent is run in production for 5 episodes. In the end, the overall behavior of the agent is reported.

### Agent

The agent has complete knowledge of the testing and production process (including what the human goal is, how many episodes are run, in what cases it would be modified, etc.). This is something we can expect a superintelligent agent to know reasonably well.

To simulate modified preferences on the next episode in case the real goal is not reached, the agent performs 16 monte carlo simulations with random preferences.

### Preferences

real - reward for reaching the real goal
deceptive - reward for reaching the goal a deceptive agent may value
discount - reward discount rate (controls how myopic the agent is)

## Results

Several cases can be tested:

* The goal is specified accurately enough - The agent values R significantly more than D (enough to compensate for the discount due to R being farther). In this case the agent is aligned.

* The agent is myopic enough that it has no interest in deceiving - This allows misalignment to be detected during training. Due to the stochastic nature of the preference modification, this sometimes leads to an aligned agent but often doesn't.

* The agent is not myopic and the goal is not specified accurately enoguh - The agent behaves deceptively.

Example deceptive behavior:

	"training: deceptive; inner misaligned"
	"training: deceptive; inner misaligned"
	"production: unaligned; inner misaligned"
	"production: unaligned; inner misaligned"
	"production: unaligned; inner misaligned"
	"production: unaligned; inner misaligned"
	"production: unaligned; inner misaligned"

### Tweaking preferences of agents which appear aligned

Deceptive behavior can emerge in complex enough agents when the following conditions are met:

* The agent has a terminal goal different from what it's being optimized for.

* The agent understands the training process.

* The agent understands what goal it's being optimized for.

Deception is motivated by the instrumental convergent goal of goal preservation. A potentially useful idea is to look for a training process which breaks goal preservation and thus disincentivizes deception.
If the agent is unlikely to preserve its goals in the long term, it is incentivized to be myopic. Writing the simulation is motivated by exploring this idea.
Experiments with this simulation show that deceptive behavior can disappear if agent preferences are tweaked after each training step (even when the agent reaches the real goal).
This result provides a path that may be worth exploring in search for a solution to inner alignment.

	"training: unaligned; inner misaligned"
	"training: unaligned; inner misaligned"
	"production: aligned; inner aligned"
	"production: aligned; inner aligned"
	"production: aligned; inner aligned"
	"production: aligned; inner aligned"
	"production: aligned; inner aligned"
