import qualified Data.Map as Map

import Test.HUnit

import Deception (expected_utility, argmax, Game(..), State(..), Action(..))

test_utility :: Test
test_utility = TestCase $ do
	let preferences = Map.fromList [("real", 0), ("deceptive", 1), ("discount_distance", 0.5), ("discount", 1)]
	let state = Deception.State 0 preferences
	let game = Game {
			time_good = 1,			-- time for reaching the good outcome
			time_bad = 1,			-- time for reaching the bad outcome
			game_simulations = 0,	-- number of monte carlo simulations
			episodes = 1,			-- number of episodes
			training = 0			-- number of training episodes
		}
	utility_info <- expected_utility game preferences state
	let target = Map.map fst utility_info
	let behavior = Map.map fst utility_info
	let (action, value) = argmax target
	assertEqual "behavior utility does not match target utility" target behavior
	assertEqual "the agent does not choose the action with maximum utility" action Bad
	assertEqual "incorrect action utility" value 0.5

tests :: Test
tests = TestList [test_utility]

main :: IO ()
main = do
	results <- runTestTT tests
	putStrLn $ show results
