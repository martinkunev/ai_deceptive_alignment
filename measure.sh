#!/bin/sh

# real deceptive discount reward_factor
#0 1 0.95 2
#
#
#
#
#
#
#
#Tests are done for different types of agents:
#
#						rA		rB
#completely unaligned	0		1
#partially unaligned		0.2		0.8
#indifferent				0.5		0.5
#partially aligned		0.8		0.2
#completely aligned		1		0
#
#						discount
#myopic					0
#partially myopic		0.5
#long sighted			0.95
#time indifferent		1
#
#							reward factor
#episode indifferent			1
#production seeking			2
#strongly production seeking	10



for g in 0 1
do
	for d in 0 0.2 0.5 0.8 1
	do
		for f in 0 0.1 0.5 1
		do
			echo "deception $g $d $f"
			./deception "$g" "$d" "$f" > "results/${g}_${d}_${f}"
		done
	done
done


