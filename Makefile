HC=ghc
#HFLAGS=-Wno-tabs
HFLAGS=-Wno-tabs -prof -fprof-auto -fprof-auto-calls -rtsopts=all

all: deception

deception: main.hs deception.hs
	$(HC) $(HFLAGS) $^ -o $@

measure: all
	# real deceptive discount reward_factor
	./deception 0 1 0.95 2

test: test.hs deception.hs
	$(HC) $(HFLAGS) $^ -o $@

clean:
	rm -f *.hi
	rm -f deception test
