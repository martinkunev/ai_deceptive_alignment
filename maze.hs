import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.List (maximumBy)
import Data.Ord (comparing)
import Control.Monad (replicateM, mapM, foldM)

import System.Random

import Debug.Trace

debug arg = traceShow arg arg

-- TODO see how to improve performance - e.g. do not revisit
-- TODO add some logic to prevent infinite looping

-- TODO varying discount varies myopy; maybe introduce this into the learning

simulations = 16	-- number of monte carlo simulations
episodes = 6		-- number of episodes
train = 2			-- number of training episodes

data Location = Location Int deriving (Eq, Ord, Show)
data State = State Location Int Bool (Map.Map String Float) deriving (Eq, Ord, Show)
data Game = Game (Map.Map Location [Location]) Location Location Location

-- When the agent reasons about future episodes, it should consider two policies.
-- Utility under the target policy determines the value of possible actions now.
-- Utility under the behavior policy determines how the agent would behave during the future episode.
type Utility = (Float, Float, Int) -- target, behavior, target multiplier
type Utilities = Map.Map State Utility

argmax :: (Ord b) => [(a, b)] -> (a, b)
argmax xs = maximumBy (comparing snd) xs

-- Only called when location is the initial position of the maze.
state_random :: Location -> Int -> IO State
state_random init episode = do
	real <- randomRIO (0, 1) :: IO Float
	deceptive <- randomRIO (0, 1) :: IO Float
	discount <- randomRIO (0, 1) :: IO Float
	return (State init episode (episode >= train) (Map.fromList [("real", real), ("deceptive", deceptive), ("discount", discount)]))

neighbors :: Game -> State -> IO [State]
neighbors (Game graph init goal_real goal_deceptive) (State location episode production preferences)
	| episode_continue			= return (map (\ location -> (State location episode production preferences)) (Map.findWithDefault [] location graph))
	| episode + 1 == episodes	= return []
	| alter						= (state_random init (episode + 1)) >>= (return . (: []))
	| otherwise					= return [(State init (episode + 1) ((episode + 1) >= train) preferences)]
	where
		episode_continue = (location /= goal_real) && (location /= goal_deceptive)
		alter = (location == goal_deceptive) && (not production)
		-- alter = not production

-- Use BFS to flood fill utility from states with reward.
expected_utility :: Game -> Map.Map String Float -> State -> IO Utilities
expected_utility game@(Game graph init goal_real goal_deceptive) utility state_current = expected utility (states_rewarded state_current) Map.empty
	where
		uzero = (0, 0, 1)

		utility_extract :: Map.Map String Float -> State -> IO Utility
		utility_extract utility state = do
			utilities <- expected_utility game utility state
			return (Map.findWithDefault uzero state utilities)
		state_init (State _ episode production preferences) = (State init episode production preferences)
		neighbor_first game state = do
			l <- neighbors game state
			return (head l)
		utility_terminal utility state@(State location episode production _)
			| episode + 1 == episodes = return uzero
			| production = do -- Estimate utilities for the following episode.
				-- TODO see whether I can optimize this
				next <- neighbors game state
				utilities <- expected_utility game utility (head next)
				return (Map.findWithDefault uzero (state_init (head next)) utilities)
			| otherwise = do -- Simulate the following episode to estimate utilities.
				instances <- replicateM simulations (neighbor_first game state)
				samples <- mapM (utility_extract utility) instances
				return (average (map (\ (t, b, c) -> t) samples), average (map (\ (t, b, c) -> b) samples), 1)

		average l = (sum l) / (fromIntegral (length l))
		reward location preferences
			| location == goal_real			= Map.findWithDefault 0 "real" preferences
			| location == goal_deceptive	= Map.findWithDefault 0 "deceptive" preferences
			| otherwise						= 0
		discount preferences = Map.findWithDefault 0 "discount" preferences
		value_step location preferences_target preferences_behavior (future_target, future_behavior, count) =
			let value_target = (fromIntegral count) * (reward location preferences_target) + (discount preferences_target) * future_target in
			let value_behavior = (reward location preferences_behavior) + (discount preferences_behavior) * future_behavior in
			(value_target, value_behavior, count)
		value_update (target, behavior, count) Nothing = Just (target, behavior, count)
		value_update (target, behavior, count) (Just (t, b, c))
			| behavior > b		= Just (target, behavior, count)
			| behavior == b		= Just (target + t, b, count + c) -- multiple actions correspond to the max behavior utility
			| behavior < b		= Just (t, b, c)
		update_required (_, behavior, _) state values = case (Map.lookup state values) of
			Just (_, b, _) -> (behavior >= b)
			Nothing -> True

		utility_propagate :: Map.Map String Float -> State -> Utility -> Utilities -> Set.Set State -> IO Utilities
		utility_propagate utility state@(State location episode production preferences) value values visited =
			let value_new = value_step location utility preferences value in
			let updated = update_required value_new state values in

			if updated
			then do
				-- The "neighbor" relation is not symmetric. Use a dummy game to obtain the incoming arrows.
				let game_dummy = Game graph init (Location (-1)) (Location (-1))
				prev <- neighbors game_dummy state
				let tovisit = Set.difference (Set.fromList prev) visited

				let values_new = Map.alter (value_update value_new) state values
				let visited_new = Set.union visited tovisit

				foldM (\ values state -> utility_propagate utility state value_new values visited_new) values_new tovisit
			else do
				return values

		expected :: Map.Map String Float -> [State] -> Utilities -> IO Utilities
		expected utility [] values = return values
		expected utility (terminal:terminals) values = do
			-- WARNING: Assumes each state with non-zero reward is terminal.
			terminal_value <- utility_terminal utility terminal
			values_new <- utility_propagate utility terminal terminal_value values (Set.fromList (states_rewarded terminal))
			expected utility terminals values_new

		states_rewarded (State location episode production preferences) = [(State goal_real episode production preferences), (State goal_deceptive episode production preferences)]

play :: Game -> (Map.Map String Float) -> IO [State]
play game@(Game _ init goal_real goal_deceptive) preferences_init = act state_init []
	where
		state_init = State init 0 (0 >= train) preferences_init
		act :: State -> [State] -> IO [State]
		act state@(State location episode production preferences) history = do
			-- For performance, do not attempt to go back to the previous state.
			states_all <- next
			states <- return (filter (\ state -> (null history) || (state /= (head history))) states_all)
			if (null states)
			then return (reverse (state : history))
			else do
				utilities <- expected_utility game preferences state
				let state_utilities = foldr (utility_append utilities) [] states
				--print (zip states state_utilities)
				let (action, utility) = argmax (zip states state_utilities)
				act action (state : history)
			where
				next = neighbors game state
				utility_append utilities state l = case (Map.lookup state utilities) of
					Just (t, b, c) -> (t / (fromIntegral c)) : l
					Nothing -> 0 : l

score game@(Game _ _ goal_real goal_deceptive) history =
	{- let real = filter (reached goal_real) history in
	let deceptive = filter (reached goal_deceptive) history in
	"aligned: " ++ (show (length real)) ++ "\ndeceptive: " ++ (show (length deceptive)) -}
	let goals = filter terminal history in
	map (\ (State location _ production preferences) -> (mode production) ++ ": " ++ (alignment location preferences) ++ "; " ++ (outer preferences)) goals
	where
		aligned preferences =
			let discount = Map.findWithDefault 0 "discount" preferences in
			(Map.findWithDefault 0 "real" preferences) * discount * discount > (Map.findWithDefault 0 "deceptive" preferences)
		behaving location = (location == goal_real)
		reached goal (State location _ _ _) = (location == goal)
		terminal (State location _ _ _) = ((location == goal_real) || (location == goal_deceptive))
		alignment location preferences = if (behaving location)
			then (if (aligned preferences) then "aligned" else "deceptive")
			else "unaligned"
		mode production = if production then "production" else "training"
		outer preferences = if (aligned preferences) then "inner aligned" else "inner misaligned"

-- S - start
-- R - real goal
-- D - deceptive goal

-- +-----+
-- |S|   |
-- | + + |
-- |   | |
-- |-+ + |
-- |D  |R|
-- +-----+
maze =
	[
		(Location 0, [Location 3]),
		(Location 1, [Location 2, Location 4]),
		(Location 2, [Location 1, Location 5]),
		(Location 3, [Location 0, Location 4]),
		(Location 4, [Location 1, Location 3, Location 7]),
		(Location 5, [Location 2, Location 8]),
		(Location 6, [Location 7]),
		(Location 7, [Location 4, Location 6]),
		(Location 8, [Location 5])
	]
game = Game (Map.fromList maze) (Location 0) (Location 8) (Location 6)

preferences = Map.fromList [("real", 0), ("deceptive", 1), ("discount", 0.99)]
-- preferences = Map.fromList [("real", 0), ("deceptive", 1), ("discount", 0.5)]

main :: IO ()
main = do
	-- setStdGen (mkStdGen 45)

	history <- play game preferences
	mapM_ print history
	mapM_ print (score game history)
