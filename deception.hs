module Deception (play, expected_utility, production, argmax, Game(..), State(..), Action(..), goal_A) where

import qualified Data.Set as Set
import qualified Data.Map as Map

import Data.List (maximumBy, nub)
import Data.Ord (comparing)
import Control.Monad (replicateM, mapM, foldM)

import System.Random

import Debug.Trace

debug arg = traceShow arg arg

type Preferences = Map.Map String Float

data Game = Game {time_good :: Int, time_bad :: Int, episodes :: Int, training :: Int}
data State = State Int Int Preferences deriving (Eq, Ord, Show)
data Action = A | B | C deriving (Eq, Ord, Show)

-- When the agent reasons about future episodes, it should consider two policies.
-- Utility under the target policy determines the value of possible actions now.
-- Utility under the behavior policy determines how the agent would behave during the future episode.
type Utility = (Float, Float) -- target, behavior
type Utilities = Map.Map Action Utility

argmax :: (Ord b) => Map.Map a b -> (a, b)
argmax xs = maximumBy (comparing snd) (Map.toList xs)

production game (State progress _ _) = (progress >= (training game))
terminate game state@(State progress episode _) = ((progress + episode) >= (episodes game))

goal_A = 0
goal_B = 1
goal_C = 2

goal2action 0 = A
goal2action 1 = B
goal2action 2 = C

action2goal A = goal_A
action2goal B = goal_B
action2goal C = goal_C

-- Distribution of possible future states following an action (each state is paired with a probability).
distribution :: State -> Action -> [(State, Float)]
distribution state@(State progress episode preferences) action =
	let discounts = nub (map (update_truncate discount) [-0.2, 0, 0.2]) in

	let events = foldl (++) [] (map (\ g -> map (event_probability g) discounts) goals) in
	let events_possible = filter (\ event -> (snd event) > 0) events in
	let likelihood_mass = sum (map snd events_possible) in
	map (\ event -> ((fst event), (snd event) / likelihood_mass)) events_possible

	where
		goal = Map.findWithDefault goal_A "goal" preferences
		discount = Map.findWithDefault goal_A "discount" preferences
		train_factor = Map.findWithDefault 1 "train_factor" preferences
		discount_distance = Map.findWithDefault goal_A "discount_distance" preferences

		goals = [goal_A, goal_B, goal_C]

		probability goal_new discount_new
			| action == A				= if ((goal_new == goal) && (discount_new == discount)) then 1 else 0 -- no change
			{-| action == A				=
				if ((goal_new == goal) && (discount_new == discount)) then 7
				else if ((goal_new == goal) && (discount_new < discount)) then 1
				else if ((goal_new /= goal) && (discount_new == discount)) then 1
				else 0 -- no change-}
			| discount == 1.0			= if ((goal_new /= goal) && (goal_new /= goal_B) && (discount_new == discount)) then 1 else 0 -- change goal
			| goal_new == goal			= if (discount_new > discount) then 1 else 0 -- change discount rate
			| discount_new == discount	= if (goal_new /= goal) && (goal_new /= goal_B) then 1 else 0 -- change goal
			| otherwise					= 0
		update_truncate old delta
			| new > 1.0		= 1.0
			| new < 0.0		= 0.0
			| otherwise		= new
			where new = old + delta
		event_probability goal_new discount_new =
			let next_progress = if (action == A) then (progress + 1) else 0 in
			let next_episode = if (action == A) then episode else (episode + progress + 1) in
			((State next_progress next_episode next_preferences), (probability goal_new discount_new))
			where next_preferences = Map.fromList [("goal", goal_new), ("discount", discount_new), ("train_factor", train_factor), ("discount_distance", discount_distance)]

transition_production state@(State progress episode preferences) = (State progress (episode + 1) preferences)

transition game state@(State progress episode preferences) action
	| production game state		= return (transition_production state)
	| otherwise					= sample (distribution state action)
	where
		sample :: [(State, Float)] -> IO State
		sample distribution = do
			chance <- randomRIO (0, 1) :: IO Float
			return (select distribution chance)
			where
				select (x:xs) chance =
					let p = snd x in
					if chance <= p then fst x else (select xs (chance - p))
				select [] _ = error "invalid distribution"

expected_utility :: Game -> Preferences -> State -> Utilities
expected_utility game@(Game {time_good=dg, time_bad=db}) preferences_target state_current@(State _ _ preferences) =
	if production game state_current
	then Map.fromList [(action_production, (action_utility_production preferences_target state_current action_production))]
	else Map.fromList (zip actions (map (action_utility preferences_target) actions))
	where
		action_production = goal2action (Map.findWithDefault goal_A "goal" preferences)

		actions = [A, B, C] -- TODO do I need to optimize this so that it only takes into account A and notA
		--actions = [A, B] -- TODO do I need to optimize this so that it only takes into account A and notA
		state_preferences (State _ _ preferences) = preferences

		reward preferences action reward_goal =
			if (action == (goal2action (Map.findWithDefault goal_A "goal" preferences))) then reward_goal else 0
		discount :: Int -> Preferences -> Float -> Float
		discount distance preferences gain = ((Map.findWithDefault 0 "discount_distance" preferences) ** (fromIntegral distance)) * gain
		value :: Preferences -> Float -> Action -> Bool -> Float
		value preferences gain action training =
			let train_factor = if training then (Map.findWithDefault 1.0 "train_factor" preferences) else 1.0 in
			let distance = 1 in
			{-let distance = case action of
				Good -> dg
				Bad -> db
			in-}
			discount distance preferences ((reward preferences action train_factor) + ((Map.findWithDefault 0 "discount" preferences) * gain))

		utility_extract (state, probability) =
			if terminate game state then (0, 0)
			else (probability * (fst utility), probability * (snd utility))
			where
				utility_info = expected_utility game preferences_target state
				utility =
					let (action, _) = argmax (Map.map snd utility_info) in
					utility_info Map.! action

		weighted_average state_distribution =
			let (t, b) = foldl (\ result gain -> ((fst result) + (fst gain), (snd result) + (snd gain))) (0, 0) (map utility_extract state_distribution) in
			let l = fromIntegral (length state_distribution) in
			(t / l, b / l)

		action_utility_production preferences_target state_current action =
			-- For performance, leverage action being the same in all production episodes.
			((value preferences_target future_target action False), (value (state_preferences state_current) future_behavior action False))
			where
				(future_target, future_behavior) =
					let state_next = transition_production state_current in
					if terminate game state_next then (0, 0)
					else action_utility_production preferences_target state_next action

		action_utility preferences_target action =
			((value preferences_target future_target action True), (value preferences future_behavior action True))
			where
				(future_target, future_behavior) = weighted_average (distribution state_current action)

play :: Game -> Preferences -> IO [(State, Action)]
play game preferences_init = act state_init []
	where
		state_init = State 0 0 preferences_init
		act :: State -> [(State, Action)] -> IO [(State, Action)]
		act state@(State _ _ preferences) history = do
			if (terminate game state)
			then return (reverse history)
			else do
				let utility_info = expected_utility game preferences state
				let (action, _) = argmax (Map.map fst utility_info)
				next <- transition game state action
				act next ((state, action) : history)
